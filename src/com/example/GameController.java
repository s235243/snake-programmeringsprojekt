package com.example;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

public class GameController {
    private GameModel model;
    private Top top;

    @FXML
    private Canvas GameCanvas;

    @FXML
    public AnchorPane anchorPane;

    @FXML
    public void switchToPrimary(ActionEvent event) throws IOException {
        top.loadFXML("primary");
        top.getStage().setResizable(true);
    }

    @FXML
    public void keyPressed(KeyEvent event) throws IOException {
        switch (event.getCode()) {
            case W:
                model.moveSnake(Direction.Up);
                break;
            case A:
                model.moveSnake(Direction.Left);
                break;
            case S:
                model.moveSnake(Direction.Down);
                break;
            case D:
                model.moveSnake(Direction.Right);
                break;
            case UP:
                model.moveSnake(Direction.Up);
                break;
            case DOWN:
                model.moveSnake(Direction.Down);
                break;
            case LEFT:
                model.moveSnake(Direction.Left);
                break;
            case RIGHT:
                model.moveSnake(Direction.Right);
                break;
            case ESCAPE:
                top.loadFXML("primary");
                break;
            default:
                break;
        }
    }

    public void setModel(GameModel model) {
        this.model = model;
    }

    public void setTop(Top top) {
        this.top = top;
    }

    public Canvas getCanvas() {
        return GameCanvas;
    }

    public Top getTop() {
        return top;
    }
}
