package com.example;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

public class PrimaryView {

    private PrimaryController controller;
    private Scene scene;

    public PrimaryView(Top top) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(Top.class.getResource("primary.fxml"));

        this.scene = new Scene(fxmlLoader.load());

        this.controller = fxmlLoader.getController();

        controller.setTop(top);

    }

    public Scene getScene() {
        return scene;
    }

    public PrimaryController getController() {
        return controller;
    }
}