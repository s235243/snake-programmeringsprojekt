package com.example;

import java.io.IOException;
import java.util.ArrayList;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Window;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.canvas.*;

public class GameView {

    private GameController controller;
    private GameModel model;
    private Scene scene;
    private GraphicsContext gc;
    private Canvas GameCanvas;
    private int width;
    private int height;
    private int cellSize;
    private Top top;

    public GameView(Top top) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(Top.class.getResource("game.fxml"));

        this.scene = new Scene(fxmlLoader.load());
        this.top = top;
        this.controller = fxmlLoader.getController();
        this.GameCanvas = controller.getCanvas();
        this.gc = controller.getCanvas().getGraphicsContext2D();
        this.model = new GameModel(this, top);

        controller.setModel(model);
        this.GameCanvas = controller.getCanvas();
        controller.setTop(top);

    }

    public void setDimensions(int width, int height) {
        this.width = width;
        this.height = height;

        calculateCellSize();

        top.getStage().setResizable(false);

        GameCanvas.setHeight(height * cellSize);
        GameCanvas.setWidth(width * cellSize);
        Window window = scene.getWindow();

        int topDecorator = (int) (window.getHeight() - scene.getHeight());
        int sideDecorator = (int) (window.getWidth() - scene.getWidth());

        top.setStageSize((int) GameCanvas.getWidth() + sideDecorator, (int) GameCanvas.getHeight() + topDecorator);

        model.setDimensions(width, height);

    }

    public void calculateCellSize() {
        Rectangle2D screenBounds = Screen.getPrimary().getBounds();
        int maxX = (int) screenBounds.getMaxX() - 100;
        int maxY = (int) screenBounds.getMaxY() - 50;
        cellSize = Math.min(maxX / width, maxY / height);

    }

    public void drawSnake(Snake snake) {
        ArrayList<Point> body = snake.getBodyList();

        gc.setFill(Color.WHITE);
        for (int i = 0; i < body.size(); i++) {
            gc.fillRect(body.get(i).getX() * cellSize, body.get(i).getY() * cellSize,
                    cellSize, cellSize);

        }

    }

    public void drawMap() {
        gc.setFill(Color.PURPLE);
        gc.fillRect(0, 0, width * cellSize, height * cellSize);
        gc.setFill(Color.MAGENTA);

        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                if ((i + j) % 2 == 0) {
                    gc.fillRect(i * cellSize, j * cellSize, cellSize, cellSize);
                }

            }
        }
        gc.setStroke(Color.GREEN);
        gc.strokeLine(0, 0, width * cellSize, 0);
        gc.strokeLine(0, 0, 0, height * cellSize);
        gc.strokeLine(GameCanvas.getWidth(), 0, GameCanvas.getWidth() - 2, GameCanvas.getHeight() + 100);
        gc.strokeLine(0, height * cellSize, width * cellSize, GameCanvas.getHeight());

    }

    public void drawFood(Point[] foodArray) {

        gc.setFill(Color.RED);

        for (Point food : foodArray) {

            gc.fillOval(food.getX() * cellSize, food.getY() * cellSize, cellSize, cellSize);

        }

    }

    public void setCanvas(Canvas GameCanvas) {
        this.GameCanvas = GameCanvas;
    }

    public Canvas getCanvas() {
        return GameCanvas;
    }

    // nyttigt når top skal udføre noget som stage.setScene(secondaryview.getscene)
    public Scene getScene() {
        return scene;
    }

    public void setGraphicsContext(GraphicsContext gc) {
        this.gc = gc;
    }

    public GameModel getModel() {
        return model;
    }

    public GameController getController() {
        return controller;
    }
}