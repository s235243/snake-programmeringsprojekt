package com.example;

import java.util.ArrayList;

public class Snake {

    private ArrayList<Point> body = new ArrayList<>();
    private Direction direction = Direction.Left;

    public Snake(Point point) {

        body.add(0, point);

        body.add(new Point(point.getX() + 1, point.getY()));

    }

    public Point headPoint() {
        return body.get(0);
    }

    public ArrayList<Point> getBodyList() {
        return body;
    }

    public Direction getDirection() {
        return direction;
    }

    public void moveTo(Point point) {
        body.add(0, point);
        body.remove(body.size() - 1);
    }

    public void eat(Point point) {
        body.add(0, point);
    }

    public void move(Direction direction) {

        Point newHead = new Point(headPoint());

        switch (direction) {
            case Up:
                newHead.setY(headPoint().getY() - 1);
                break;
            case Down:
                newHead.setY(headPoint().getY() + 1);
                break;
            case Left:
                newHead.setX(headPoint().getX() - 1);
                break;
            case Right:
                newHead.setX(headPoint().getX() + 1);
                break;
            default:
                System.out.println("Unexpected direction " + direction);
        }

        this.direction = direction;

        body.add(0, newHead);
        body.remove(body.size() - 1);
    }

    public void move() {
        move(direction);
    }

    public Point destination() {
        return destination(direction);
    }

    public Point destination(Direction direction) {

        Point destination = new Point(headPoint());

        switch (direction) {
            case Up:
                destination.setY(destination.getY() - 1);
                break;
            case Down:
                destination.setY(destination.getY() + 1);
                break;
            case Left:
                destination.setX(destination.getX() - 1);
                break;
            case Right:
                destination.setX(destination.getX() + 1);
                break;
            default:
                System.out.println("unexpected direciton" + direction);
                break;
        }
        return destination;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public boolean inBody(Point point) {
        for (Point bodycell : body) {
            if (point.getX() == bodycell.getX() && point.getY() == bodycell.getY()) {
                return true;
            }
        }
        return false;
    }

    public boolean hasHitSelf() {
        for (int i = 1; i < body.size(); i++) {

            if (headPoint().getX() == body.get(i).getX() && headPoint().getY() == body.get(i).getY()) {
                return true;
            }

        }
        return false;
    }

    public String toString() {
        String string = "___ \n";
        for (int i = 0; i < body.size(); i++) {
            string += body.get(i);
            string += "\n";
        }
        return string;
    }
}