package com.example;

public enum Direction {
    Up,
    Down,
    Left,
    Right
}
