package com.example;

import java.util.Random;

public class Point {

    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point point) {
        this.x = point.getX();
        this.y = point.getY();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean samePos(Point point) {
        if (x == point.getX() && y == point.getY()) {
            return true;
        } else {
            return false;
        }
    }

    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void moveRandom(int boundX, int boundY) {
        Random random = new Random();
        setX(random.nextInt(boundX));
        setY(random.nextInt(boundY));
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String toString() {
        return x + ", " + y;
    }
}
