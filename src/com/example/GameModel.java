package com.example;

import java.io.IOException;

public class GameModel {
    private Snake snake;
    private GameView view;
    private Top top;
    private Point[] foodArray;
    private int width;
    private int height;

    // Change size and point
    public GameModel(GameView view, Top top) {

        this.view = view;
        this.top = top;
    }

    public void spawnFood() {
        for (Point food : foodArray) {
            while (snake.inBody(food)) {
                food.moveRandom(width, height);
            }
        }
    }

    public void spawnFood(int numFood) {

        this.foodArray = new Point[numFood];

        for (int i = 0; i < foodArray.length; i++) {
            foodArray[i] = new Point(-1, -1);
            foodArray[i].moveRandom(width, height);

            while (snake.inBody(foodArray[i])) {
                foodArray[i].moveRandom(width, height);
            }
        }
    }

    public void setDimensions(int width, int height) {
        this.width = width;
        this.height = height;
        this.snake = new Snake(new Point(width / 2, height / 2));
        spawnFood(2);

        view.drawMap();
        view.drawSnake(snake);
        view.drawFood(foodArray);
    }

    public Snake getSnake() {
        return snake;
    }

    public void moveSnake(Direction direction) throws IOException {

        // set snake direction and destination point
        switch (direction) {
            case Up:
                if (snake.getDirection() != Direction.Down) {
                    snake.setDirection(direction);
                }

                break;
            case Right:
                if (snake.getDirection() != Direction.Left) {
                    snake.setDirection(direction);
                }

                break;
            case Down:
                if (snake.getDirection() != Direction.Up) {
                    snake.setDirection(direction);
                }
                break;
            case Left:
                if (snake.getDirection() != Direction.Right) {
                    snake.setDirection(direction);
                }
                break;
            default:
                break;
        }

        Point destination = snake.destination();

        // alters snake destination if its out of bounds
        try {
            switch (outOfBounds(destination)) {
                case Up:
                    destination.setY(height - 1);
                    break;
                case Down:
                    destination.setY(0);
                    break;
                case Left:
                    destination.setX(width - 1);
                    break;
                case Right:
                    destination.setX(0);
                    break;
                default:
                    break;
            }
        } catch (NullPointerException e) {
            // acts as a case null:
        }

        // evaluates whats at the snakes destination
        if (snake.inBody(destination)) {
            System.out.println("You died.");
            top.loadFXML("primary");

        } else if (onFood(destination)) {

            snake.eat(destination);
            spawnFood();

        } else {
            snake.moveTo(destination);
        }

        view.drawMap();
        view.drawSnake(snake);
        view.drawFood(foodArray);
    }

    public boolean onFood(Point head) {
        for (Point food : foodArray) {
            if (head.getX() == food.getX() && head.getY() == food.getY()) {
                return true;
            }
        }
        return false;
    }

    public Direction outOfBounds(Point point) {
        if (point.getX() >= width) {
            return Direction.Right;
        } else if (point.getX() < 0) {
            return Direction.Left;
        } else if (point.getY() >= height) {
            return Direction.Down;
        } else if (point.getY() < 0) {
            return Direction.Up;
        } else {
            return null;
        }
    }
}
