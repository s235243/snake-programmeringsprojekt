package com.example;

import java.io.IOException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.control.Label;

public class PrimaryController {

    private Top top;

    @FXML
    private Slider heightSlider;

    @FXML
    private Slider widthSlider;

    @FXML
    private Label heightLabel;

    @FXML
    private Label widthLabel;

    @FXML
    public void switchToSecondary(ActionEvent event) throws IOException {
        top.loadFXML("game");
        top.getGameView().setDimensions((int) widthSlider.getValue(), (int) heightSlider.getValue());
    }

    public void setSliders(int width, int height) {

        heightSlider.setValue(height);
        widthSlider.setValue(width);

        heightLabel.textProperty().setValue(Integer.toString(height));
        widthLabel.textProperty().setValue(Integer.toString(width));

        heightSlider.valueProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                heightLabel.textProperty().setValue(String.valueOf(newValue.intValue()));
            }

        });

        widthSlider.valueProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                widthLabel.textProperty().setValue(String.valueOf(newValue.intValue()));
            }
        });

    }

    public void setTop(Top top) {
        this.top = top;
    }
}
