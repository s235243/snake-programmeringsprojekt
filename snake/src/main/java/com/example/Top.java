package com.example;

import javafx.application.Application;

import javafx.stage.Stage;
import java.io.IOException;
import java.util.List;

/**
 * JavaFX App
 */
public class Top extends Application {

    private Stage stage;

    private GameView gameView;
    private PrimaryView primaryView;

    @Override
    public void start(Stage stage) throws IOException {
        Parameters p = this.getParameters();
        List<String> rawArgumentsList = p.getRaw();

        this.stage = stage;

        this.primaryView = new PrimaryView(this);
        this.gameView = new GameView(this);

        int width = 15;
        int height = 15;
        if (rawArgumentsList.size() == 2 && isIntegers(rawArgumentsList)) {
            width = Integer.valueOf(rawArgumentsList.get(0));
            height = Integer.valueOf(rawArgumentsList.get(1));
            if (width > 100 || width < 5) {
                width = 15;
            }
            if (height > 100 || height < 5) {
                height = 15;
            }
        }
        primaryView.getController().setSliders(width, height);

        loadFXML("primary");

        stage.show();

    }

    public void setStageSize(int width, int height) {
        stage.setWidth(width);
        stage.setHeight(height);
        stage.centerOnScreen();
    }

    public Stage getStage() {
        return stage;
    }

    public GameView getGameView() {
        return gameView;
    }

    public void loadFXML(String fxml) throws IOException {

        switch (fxml) {
            case "primary":
                stage.setScene(primaryView.getScene());
                stage.setWidth(640);
                stage.setHeight(480);
                break;

            case "game":
                stage.setScene(gameView.getScene());
                gameView.getCanvas().requestFocus();
                break;
            default:
                System.out.println("Unexpected input " + fxml);
                break;
        }
    }

    public static boolean isIntegers(List<String> s) {
        try {

            for (String string : s) {
                int n = Integer.parseInt(string);
            }
            return true;

        } catch (NumberFormatException e) {
            return false;
        }

    }

    public static void main(String[] args) {
        launch(args);
    }

    /*
     * public void loadFXMLSecondary() throws IOException {
     * GameView gameView = new GameView(this);
     * this.stage.setScene(gameView.getScene());
     * }
     * 
     * public void loadFXMLPrimary() throws IOException {
     * PrimaryView primary = new PrimaryView(this);
     * this.stage.setScene(primary.getScene());
     * }
     */
}